package org.example;

import org.example.gui.component.TabViewComponent;
import org.example.gui.window.MainWindow;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame frame = new MainWindow("Probability Theory Practical 5 (by Zerumi)");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            //Add content to the window.
            frame.add(new TabViewComponent(), BorderLayout.CENTER);

            //Display the window.
            frame.pack();
            frame.setVisible(true);
        });
    }
}