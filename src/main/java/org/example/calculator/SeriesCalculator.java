package org.example.calculator;

import org.apache.commons.lang3.Range;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public record SeriesCalculator(ArrayList<BigDecimal> sourceSeries) {
    public SeriesCalculator {
        if (sourceSeries.isEmpty()) throw new IllegalArgumentException("Empty list");
    }

    public ArrayList<BigDecimal> variationalSeries() {
        return new ArrayList<>(sourceSeries.stream().sorted().toList());
    }

    public BigDecimal sampleRange() {
        ArrayList<BigDecimal> sorted = variationalSeries();
        return sorted.get(sorted.size() - 1).subtract(sorted.get(0)); // sorted[size - 1] - sorted[0]
    }

    public LinkedHashMap<BigDecimal, Integer> statSeries() {
        LinkedHashMap<BigDecimal, Integer> result = new LinkedHashMap<>();

        for (BigDecimal sourceElement : variationalSeries()) {
            result.merge(sourceElement, 1, Integer::sum);
        }

        return result;
    }

    public LinkedHashMap<BigDecimal, BigDecimal> statSeriesFreq() {
        LinkedHashMap<BigDecimal, BigDecimal> result = new LinkedHashMap<>();

        LinkedHashMap<BigDecimal, Integer> statSeries = statSeries();

        long sum = 0;
        for (BigDecimal sourceElement : statSeries.keySet()) {
            sum += statSeries.get(sourceElement);
        }

        for (BigDecimal sourceElement : statSeries.keySet()) {
            result.put(sourceElement, BigDecimal.valueOf(statSeries.get(sourceElement)).setScale(32, RoundingMode.HALF_UP)
                    .divide(BigDecimal.valueOf(sum), RoundingMode.HALF_UP));
        }

        return result;
    }

    public BigDecimal stedgersFormula() {
        int scale = 32;
        BigDecimal size = BigDecimal.valueOf(sourceSeries.size());
        BigDecimal two = BigDecimal.valueOf(2);
        BigDecimal one = BigDecimal.valueOf(1);

        BigDecimal sizeLn = BigDecimalMath.ln(size, scale);
        BigDecimal twoLn = BigDecimalMath.ln(two, scale);

        BigDecimal sizeLogTwo = sizeLn.divide(twoLn, RoundingMode.HALF_UP);
        BigDecimal sizeLogTwoPlusOne = sizeLogTwo.add(one);

        BigDecimal sampleRange = sampleRange().setScale(scale, RoundingMode.HALF_UP);

        return sampleRange.divide(sizeLogTwoPlusOne, RoundingMode.HALF_UP);
    }

    public LinkedHashMap<Range<BigDecimal>, BigDecimal> empiricalDistF() {
        LinkedHashMap<BigDecimal, Integer> statSeries = statSeries();

        LinkedHashMap<Range<BigDecimal>, BigDecimal> result = new LinkedHashMap<>();
        long sumOfValues = 0;
        BigDecimal prevKey = null;
        for (BigDecimal key : statSeries.keySet()) {
            if (prevKey == null)
                prevKey = key;
            BigDecimal value = BigDecimal.valueOf((double) (sumOfValues) / sourceSeries.size());
            sumOfValues += statSeries.get(key);
            result.put(Range.between(prevKey, key), value);
            prevKey = key;
        }
        if (prevKey != null)
            result.put(Range.between(prevKey, prevKey), BigDecimal.ONE);

        return result;
    }

    public LinkedHashMap<Range<BigDecimal>, Long> freqAmount() {
        LinkedHashMap<Range<BigDecimal>, Long> result = new LinkedHashMap<>();
        LinkedHashMap<BigDecimal, Integer> statSeries = statSeries();
        BigDecimal firstValue = variationalSeries().get(0);
        BigDecimal h = stedgersFormula();
        BigDecimal hDiv2 = h.divide(BigDecimal.valueOf(2.0), RoundingMode.HALF_UP);
        BigDecimal startValue = firstValue.subtract(hDiv2);
        BigDecimal lastValue = variationalSeries().get(variationalSeries().size() - 1);
        for (BigDecimal startOfRange = startValue;
             startOfRange.compareTo(lastValue) < 0;
             startOfRange = startOfRange.add(h)) {
            BigDecimal endOfRange = startOfRange.add(h);
            Range<BigDecimal> resKey = Range.between(startOfRange, endOfRange);
            long currentFreq = 0;
            for (BigDecimal key : statSeries.keySet()) {
                if (key.compareTo(startOfRange) >= 0 && key.compareTo(endOfRange) < 0)
                    currentFreq += statSeries.get(key);
            }
            Long resVal = currentFreq;
            result.put(resKey, resVal);
        }

        return result;
    }

    public LinkedHashMap<Range<BigDecimal>, BigDecimal> freqHistogramData() {
        LinkedHashMap<Range<BigDecimal>, BigDecimal> result = new LinkedHashMap<>();
        LinkedHashMap<Range<BigDecimal>, Long> amount = freqAmount();
        for (Range<BigDecimal> key : amount.keySet()) {
            result.put(key, BigDecimal.valueOf(amount.get(key)).setScale(32, RoundingMode.HALF_UP)
                    .divide(stedgersFormula(), RoundingMode.HALF_UP));
        }
        return result;
    }

    public BigDecimal mathAvg() {
        LinkedHashMap<BigDecimal, Integer> statSeries = statSeries();

        BigDecimal sum = BigDecimal.ZERO;

        for (BigDecimal key : statSeries.keySet()) {
            BigDecimal value = BigDecimal.valueOf(statSeries.get(key)).setScale(32, RoundingMode.HALF_UP);
            BigDecimal product = key.multiply(value);
            sum = sum.add(product);
        }

        return sum.divide(BigDecimal.valueOf(sourceSeries.size()), RoundingMode.HALF_UP);
    }

    public BigDecimal mathDispersion() {
        LinkedHashMap<BigDecimal, Integer> statSeries = statSeries();

        BigDecimal sum = BigDecimal.ZERO;

        for (BigDecimal key : statSeries.keySet()) {
            BigDecimal value = BigDecimal.valueOf(statSeries.get(key)).setScale(32, RoundingMode.HALF_UP);
            BigDecimal product = key.multiply(key).multiply(value);
            sum = sum.add(product);
        }

        BigDecimal mAvgSq = mathAvg().multiply(mathAvg());

        return sum.divide(BigDecimal.valueOf(sourceSeries.size()), RoundingMode.HALF_UP).subtract(mAvgSq);
    }

    public BigDecimal stdDeviation() {
        return BigDecimalMath.sqrt(mathDispersion());
    }
}
