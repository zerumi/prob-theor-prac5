package org.example.gui.adapter;

import org.apache.commons.lang3.Range;
import org.example.calculator.SeriesCalculator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public class ConfigureTextOutput {
    public static String textOutput(SeriesCalculator calculator) {
        StringBuilder builder = new StringBuilder();
        builder.append("Source series: ").append(calculator.sourceSeries().stream().map(Object::toString)
                .collect(Collectors.joining(", ")));
        builder.append("\n\n");
        builder.append("Variation Series: ").append(calculator.variationalSeries().stream().map(Object::toString)
                .collect(Collectors.joining(", ")));
        builder.append("\n\n");
        builder.append("Sample Range: ").append(calculator.sampleRange());
        builder.append(" / Minimum: ").append(calculator.variationalSeries().get(0));
        builder.append(" / Maximum: ").append(calculator.variationalSeries()
                .get(calculator.variationalSeries().size() - 1));
        builder.append("\n\nStat Series:\n");
        LinkedHashMap<BigDecimal, Integer> statSeries = calculator.statSeries();
        LinkedHashMap<BigDecimal, BigDecimal> statFreq = calculator.statSeriesFreq();
        for (BigDecimal key : statSeries.keySet()) {
            builder.append(key).append(": Amount: ").append(statSeries.get(key))
                    .append(" / Freq: ").append(statFreq.get(key).stripTrailingZeros())
                    .append("\n");
        }
        builder.append("\n");
        builder.append("Average: ").append(calculator.mathAvg().stripTrailingZeros())
                .append(" / Dispersion: ").append(calculator.mathDispersion().stripTrailingZeros())
                .append(" / Standard deviation: ").append(calculator.stdDeviation().stripTrailingZeros());
        builder.append("\n\nEmpirical F:\n");
        LinkedHashMap<Range<BigDecimal>, BigDecimal> f = calculator.empiricalDistF();
        boolean fistIteration = true;
        for (Range<BigDecimal> key : f.keySet()) {
            if (key.getMaximum().equals(key.getMinimum())) {
                if (fistIteration) {
                    builder.append("below ");
                    fistIteration = false;
                } else
                    builder.append("above ");
                builder.append(key.getMinimum()).append(": ").append(f.get(key)).append("\n");
            } else
                builder.append(key).append(": ").append(f.get(key)).append("\n");
        }
        builder.append("\nInterval Stat Series:\n");
        LinkedHashMap<Range<BigDecimal>, Long> freqAmount = calculator.freqAmount();
        LinkedHashMap<Range<BigDecimal>, BigDecimal> freq = calculator.freqHistogramData();
        for (Range<BigDecimal> key : freq.keySet()) {
            builder.append(key).append(": ").append("Amount: ").append(freqAmount.get(key))
                    .append(" / Frequency: ").append(freq.get(key)).append("\n");
        }
        builder.append("\n\n");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm:ss");
        builder.append("Calculated at: ").append(LocalDateTime.now().format(formatter));
        builder.append("\n").append("by Zerumi");
        return builder.toString();
    }
}
