package org.example.gui.adapter;

import org.apache.commons.lang3.Range;
import org.example.calculator.BigDecimalMath;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class DatasetAdapter {
    public static XYDataset fromEmpiricalFHashMap(LinkedHashMap<Range<BigDecimal>, BigDecimal> source) {
        XYSeries dataset = new XYSeries("1");
        for (Range<BigDecimal> key : source.keySet()) {
            dataset.add(key.getMinimum(), source.get(key));
            dataset.add(key.getMaximum(), source.get(key));
        }

        var result = new XYSeriesCollection();
        result.addSeries(dataset);

        return result;
    }

    public static XYDataset fromSeries(LinkedHashMap<Range<BigDecimal>, Long> source) {
        XYSeries dataset = new XYSeries("1");

        for (Range<BigDecimal> key : source.keySet()) {
            BigDecimal two = BigDecimal.ONE.add(BigDecimal.ONE);
            BigDecimal avg = key.getMaximum().subtract(key.getMinimum()).divide(two, RoundingMode.HALF_UP);
            dataset.add(key.getMinimum().add(avg), source.get(key));
        }

        var result = new XYSeriesCollection();
        result.addSeries(dataset);

        return result;
    }

    public static IntervalXYDataset fromFreq(LinkedHashMap<Range<BigDecimal>, BigDecimal> source) {
        HistogramDataset dataset = new HistogramDataset();
        ArrayList<Double> values = new ArrayList<>();
        boolean firstIteration = true;
        for (Range<BigDecimal> key: source.keySet()) {
            BigDecimal freq = source.get(key);
            BigDecimal amount = key.getMaximum().subtract(key.getMinimum()).divide(freq, RoundingMode.HALF_UP);
            for (BigDecimal i = BigDecimal.ZERO; i.compareTo(freq) < 0; i = i.add(BigDecimal.ONE)) {
                if (firstIteration) {
                    values.add(key.getMinimum().doubleValue());
                    firstIteration = false;
                    continue;
                }
                values.add(key.getMinimum().add(amount).doubleValue());
            }
        }
        values.remove(values.size() - 1);
        Range<BigDecimal> last = null;
        for (Map.Entry<Range<BigDecimal>, BigDecimal> rangeBigDecimalEntry : source.entrySet()) {
            last = rangeBigDecimalEntry.getKey();
        }
        if (last != null)
            values.add(last.getMaximum().doubleValue());
        dataset.addSeries("key", values.stream().mapToDouble(Double::doubleValue).toArray(), source.size());
        //dataset.addSeries("key", values, 50);
        return dataset;
    }
}
