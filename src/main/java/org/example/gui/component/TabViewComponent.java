package org.example.gui.component;

import org.example.gui.adapter.ConfigureTextOutput;
import org.example.gui.adapter.DatasetAdapter;
import org.example.gui.data.CalculatorSource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class TabViewComponent extends JPanel {
    public TabViewComponent() {
        super(new GridLayout(1, 1));

        JTabbedPane tabbedPane = new JTabbedPane();

        JComponent panel1 = makeTextPanel(ConfigureTextOutput.textOutput(CalculatorSource.getInstance().getSeriesCalculator()));
        tabbedPane.addTab("Text Output", null, panel1,
                "Text Output");
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

        JComponent panel2 = new ChartComponent(
                DatasetAdapter.fromEmpiricalFHashMap(CalculatorSource.getInstance().getSeriesCalculator().empiricalDistF()),
                "Empirical Function", "X", "F(X)");
        tabbedPane.addTab("Empirical Function", null, panel2,
                "Empirical Function");
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

        JComponent panel3 = new HistogramComponent(
                DatasetAdapter.fromFreq(CalculatorSource.getInstance().getSeriesCalculator().freqHistogramData()),
                "Frequency", "X", "frequency (rounded value)");
        tabbedPane.addTab("Frequency Histogram", null, panel3,
                "Histogram");
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);

        JComponent panel4 = new ChartComponent(
                DatasetAdapter.fromSeries(CalculatorSource.getInstance().getSeriesCalculator().freqAmount()),
                "Polygons of Frequency", "X", "N(X)");
        panel4.setPreferredSize(new Dimension(410, 50));
        tabbedPane.addTab("Polygons of Frequency", null, panel4,
                "Polygons of Frequency");
        tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);

        //Add the tabbed pane to this panel.
        add(tabbedPane);

        //The following line enables to use scrolling tabs.
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    }

    protected JComponent makeTextPanel(String text) {

        JPanel panel = new JPanel(false);
        JTextArea filler = new JTextArea(text);
        filler.setWrapStyleWord( true );
        filler.setLineWrap( true );
        filler.setEnabled( false );
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        return scrollPane;
    }
}
