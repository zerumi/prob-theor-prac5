package org.example.gui.component;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.IntervalXYDataset;

import javax.swing.*;
import java.awt.*;

public class HistogramComponent extends JPanel {
    private final IntervalXYDataset dataset;
    private final String title;
    private final String xAxisTitle;
    private final String yAxisTitle;

    public HistogramComponent(IntervalXYDataset dataset, String title, String xAxisTitle, String yAxisTitle) {
        this.dataset = dataset;
        this.title = title;
        this.xAxisTitle = xAxisTitle;
        this.yAxisTitle = yAxisTitle;
        initUI();
    }

    private void initUI() {

        JFreeChart chart = createHistogram(dataset);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        chartPanel.setBackground(Color.white);
        add(chartPanel);
    }

    private JFreeChart createHistogram(IntervalXYDataset dataset) {
        return ChartFactory
                .createHistogram(this.title,
                        this.xAxisTitle,
                        this.yAxisTitle,
                        dataset,
                        PlotOrientation.VERTICAL,
                        true,
                        true,
                        false);
    }
}
