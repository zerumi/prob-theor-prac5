package org.example.gui.data;

import org.example.calculator.SeriesCalculator;

public class CalculatorSource {

    private final SeriesCalculator seriesCalculator;
    private static CalculatorSource instance;

    private CalculatorSource() {
        seriesCalculator = new SeriesCalculator(DataSource.getInstance().getSource());
    }

    public static CalculatorSource getInstance() {
        if (instance == null)
            instance = new CalculatorSource();
        return instance;
    }

    public SeriesCalculator getSeriesCalculator() {
        return seriesCalculator;
    }
}
