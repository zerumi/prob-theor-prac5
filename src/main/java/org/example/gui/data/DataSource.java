package org.example.gui.data;

import java.math.BigDecimal;
import java.util.ArrayList;

public class DataSource {
    private static DataSource instance;

    private final ArrayList<BigDecimal> source = new ArrayList<>() {{
        add(BigDecimal.valueOf(-0.45));
        add(BigDecimal.valueOf(1.42));
        add(BigDecimal.valueOf(0.52));
        add(BigDecimal.valueOf(0.66));
        add(BigDecimal.valueOf(-1.63));
        add(BigDecimal.valueOf(-1.70));
        add(BigDecimal.valueOf(-0.42));
        add(BigDecimal.valueOf(0.17));
        add(BigDecimal.valueOf(-1.18));
        add(BigDecimal.valueOf(0.14));
        add(BigDecimal.valueOf(1.62));
        add(BigDecimal.valueOf(-1.71));
        add(BigDecimal.valueOf(0.43));
        add(BigDecimal.valueOf(-0.18));
        add(BigDecimal.valueOf(1.42));
        add(BigDecimal.valueOf(0.69));
        add(BigDecimal.valueOf(-0.55));
        add(BigDecimal.valueOf(-0.7));
        add(BigDecimal.valueOf(-1.51));
        add(BigDecimal.valueOf(-0.68));
    }};

    public ArrayList<BigDecimal> getSource() {
        return source;
    }

    private DataSource() {}

    public static DataSource getInstance() {
        if (instance == null)
            instance = new DataSource();
        return instance;
    }
}
