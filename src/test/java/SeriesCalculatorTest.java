import org.apache.commons.lang3.Range;
import org.example.calculator.SeriesCalculator;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SeriesCalculatorTest {

    final ArrayList<BigDecimal> source;

    final SeriesCalculator calculator;

    public SeriesCalculatorTest() {
        source = new ArrayList<>() {{
            add(BigDecimal.valueOf(-0.45));
            add(BigDecimal.valueOf(1.42));
            add(BigDecimal.valueOf(0.52));
            add(BigDecimal.valueOf(0.66));
            add(BigDecimal.valueOf(-1.63));
            add(BigDecimal.valueOf(-1.70));
            add(BigDecimal.valueOf(-0.42));
            add(BigDecimal.valueOf(0.17));
            add(BigDecimal.valueOf(-1.18));
            add(BigDecimal.valueOf(0.14));
            add(BigDecimal.valueOf(1.62));
            add(BigDecimal.valueOf(-1.71));
            add(BigDecimal.valueOf(0.43));
            add(BigDecimal.valueOf(-0.18));
            add(BigDecimal.valueOf(1.42));
            add(BigDecimal.valueOf(0.69));
            add(BigDecimal.valueOf(-0.55));
            add(BigDecimal.valueOf(-0.7));
            add(BigDecimal.valueOf(-1.51));
            add(BigDecimal.valueOf(-0.68));
        }};

        calculator = new SeriesCalculator(source);
    }

    @Test
    public void testVariationalSeries() {
        assert calculator.variationalSeries().equals(new ArrayList<>() {{
            add(BigDecimal.valueOf(-1.71));
            add(BigDecimal.valueOf(-1.70));
            add(BigDecimal.valueOf(-1.63));
            add(BigDecimal.valueOf(-1.51));
            add(BigDecimal.valueOf(-1.18));
            add(BigDecimal.valueOf(-0.7));
            add(BigDecimal.valueOf(-0.68));
            add(BigDecimal.valueOf(-0.55));
            add(BigDecimal.valueOf(-0.45));
            add(BigDecimal.valueOf(-0.42));
            add(BigDecimal.valueOf(-0.18));
            add(BigDecimal.valueOf(0.14));
            add(BigDecimal.valueOf(0.17));
            add(BigDecimal.valueOf(0.43));
            add(BigDecimal.valueOf(0.52));
            add(BigDecimal.valueOf(0.66));
            add(BigDecimal.valueOf(0.69));
            add(BigDecimal.valueOf(1.42));
            add(BigDecimal.valueOf(1.42));
            add(BigDecimal.valueOf(1.62));
        }});
    }

    @Test
    public void testSampleRange() {
        assert calculator.sampleRange().equals(BigDecimal.valueOf(3.33));
    }

    @Test
    public void testStatSeries() {
        assert calculator.statSeries().equals(new LinkedHashMap<>(){{
            put(BigDecimal.valueOf(-1.71), 1);
            put(BigDecimal.valueOf(-1.70), 1);
            put(BigDecimal.valueOf(-1.63), 1);
            put(BigDecimal.valueOf(-1.51), 1);
            put(BigDecimal.valueOf(-1.18), 1);
            put(BigDecimal.valueOf(-0.7), 1);
            put(BigDecimal.valueOf(-0.68), 1);
            put(BigDecimal.valueOf(-0.55), 1);
            put(BigDecimal.valueOf(-0.45), 1);
            put(BigDecimal.valueOf(-0.42), 1);
            put(BigDecimal.valueOf(-0.18), 1);
            put(BigDecimal.valueOf(0.14), 1);
            put(BigDecimal.valueOf(0.17), 1);
            put(BigDecimal.valueOf(0.43), 1);
            put(BigDecimal.valueOf(0.52), 1);
            put(BigDecimal.valueOf(0.66), 1);
            put(BigDecimal.valueOf(0.69), 1);
            put(BigDecimal.valueOf(1.42), 2);
            put(BigDecimal.valueOf(1.62), 1);
        }});
    }

    @Test
    public void testSFormula() {
        assert calculator.stedgersFormula().equals(new BigDecimal("0.62571307628132823388309393015465"));
    }

    @Test
    public void testF() {
        assert calculator.empiricalDistF().equals(new LinkedHashMap<>(){{
            put(Range.between(BigDecimal.valueOf(-1.71), BigDecimal.valueOf(-1.71)), BigDecimal.valueOf(0.00));
            put(Range.between(BigDecimal.valueOf(-1.71), BigDecimal.valueOf(-1.70)), BigDecimal.valueOf(0.05));
            put(Range.between(BigDecimal.valueOf(-1.70), BigDecimal.valueOf(-1.63)), BigDecimal.valueOf(0.10));
            put(Range.between(BigDecimal.valueOf(-1.63), BigDecimal.valueOf(-1.51)), BigDecimal.valueOf(0.15));
            put(Range.between(BigDecimal.valueOf(-1.51), BigDecimal.valueOf(-1.18)), BigDecimal.valueOf(0.20));
            put(Range.between(BigDecimal.valueOf(-1.18), BigDecimal.valueOf(-0.7)), BigDecimal.valueOf(0.25));
            put(Range.between(BigDecimal.valueOf(-0.7), BigDecimal.valueOf(-0.68)), BigDecimal.valueOf(0.30));
            put(Range.between(BigDecimal.valueOf(-0.68), BigDecimal.valueOf(-0.55)), BigDecimal.valueOf(0.35));
            put(Range.between(BigDecimal.valueOf(-0.55), BigDecimal.valueOf(-0.45)), BigDecimal.valueOf(0.40));
            put(Range.between(BigDecimal.valueOf(-0.45), BigDecimal.valueOf(-0.42)), BigDecimal.valueOf(0.45));
            put(Range.between(BigDecimal.valueOf(-0.42), BigDecimal.valueOf(-0.18)), BigDecimal.valueOf(0.50));
            put(Range.between(BigDecimal.valueOf(-0.18), BigDecimal.valueOf(0.14)), BigDecimal.valueOf(0.55));
            put(Range.between(BigDecimal.valueOf(0.14), BigDecimal.valueOf(0.17)), BigDecimal.valueOf(0.60));
            put(Range.between(BigDecimal.valueOf(0.17), BigDecimal.valueOf(0.43)), BigDecimal.valueOf(0.65));
            put(Range.between(BigDecimal.valueOf(0.43), BigDecimal.valueOf(0.52)), BigDecimal.valueOf(0.70));
            put(Range.between(BigDecimal.valueOf(0.52), BigDecimal.valueOf(0.66)), BigDecimal.valueOf(0.75));
            put(Range.between(BigDecimal.valueOf(0.66), BigDecimal.valueOf(0.69)), BigDecimal.valueOf(0.80));
            put(Range.between(BigDecimal.valueOf(0.69), BigDecimal.valueOf(1.42)), BigDecimal.valueOf(0.85));
            put(Range.between(BigDecimal.valueOf(1.42), BigDecimal.valueOf(1.62)), BigDecimal.valueOf(0.95));
            put(Range.between(BigDecimal.valueOf(1.62), BigDecimal.valueOf(1.62)), BigDecimal.ONE);
        }});
    }

    @Test
    public void testGraphData() {
        assert calculator.freqHistogramData() != null;
    }
}
